// alert("hi");

// Functions

	// function printInput(){
	// 	let nickname = prompt("Enter your nickname");
	// 	console.log("Hi " + nickname);
	// };

	// printInput();

	// Parameters and Arguments

	function printName(name){
		console.log("My name is " + name);
	};

	printName("Sean");
	//You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

	// "name" is called a parameter
	// A "parameter" acts as a named variable/container that exists only inside of a function
	// It is used to store information that is provided to a function when it is called/invoked.

	//"Sean", the information/data provided directly into the function is called an argument.
	//Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

	// When the "printName()" function is called again, it stores the value of "Shaun" in the parameter "name" then uses it to print a message.
	printName("Shaun");

	// invoked without argument
	printName() //result to undefined

	//variables can also be passed as an argument.
	let variableName = "Shawn";
	printName(variableName);


	//You can create a function which can be re-used to check for a number's divisibility instead of having to manually do it.
	function checkDivisibilityBy8(num){
		let remainder = num % 8
		console.log("The remainder of " + num + " divided by 8 is " + remainder);

		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(2548966);

	//You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Function as Arguments

	// Function parameters can also accept other functions as arguments
	// Some complex functions use other functions as arguments to perform more complicated results
	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed");
	};

	function invokeFunction(nestedFunction){
		nestedFunction();

	};

	invokeFunction(argumentFunction);

	// Adding and removing the parentheses "()" impacts the output of JavaScript heavily
	// When a function is used with parentheses "()", it denotes invoking/calling a function
	// A function used without a parenthesis is normally associated with using the function as an argument to another function

	// nested function inside a function- depends on the scope
	// function invokeFunction(nestedFunction){
	// 	nestedFunction(argumentFunction);

	// 	function nestedFunction(argumentFunction){
	// 		argumentFunction();

	// 	};
	// };

	// invokeFunction(nestedFunction);

// Using multiple parameters
	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.
	
	function createFullName(firstName, middleName, lastName){

		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	createFullName("Jane", "Dela", "Cruz");

	// "Jane" will be stored in the parameter "firstName"
	// "Dela" will be stored in the parameter "middleName"
	// "Cruz" will be stored in the parameter "lastName"
	createFullName("Cruz", "Jane", "Dela");
	
	// createFullName(prompt("Enter your first name"), prompt("Enter your middle name"), prompt("Enter your last name"));


	// In JavaScript, providing more/less arguments than the expected parameters will not return an error.

	//Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.

	// In other programming languages, this will return an error stating that "the expected number of arguments do not match the number of parameters".
	createFullName("Jake", "Castro"); // result: Jake Castro undefined

	createFullName("Jean", "Rodriguez", "Ferrer", "Hello"); //result: Jean Rodriguez Ferrer

	// using multiple variables as multiple arguments
	let firstName = "John";
	let middleName = "Reyes";
	let lastName = "Garcia";

	createFullName(firstName, middleName, lastName);


	//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

	//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.
	function printFullName(middleName, firstName, lastName){

		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	printFullName("Jan", "Asuncion", "Cruz");
	// result: Asuncion Jan Cruz because "Jan" was received as middleName, "Asuncion" was received as firstName.

	// return statement
	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

	function returnFullName(firstName, middleName, lastName){

		return firstName + " " + middleName + " " + lastName
		console.log("This message should not be printed");

	};

	// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName)

	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.


	// In this example, console.log() will print the returned value of the returnFullName() function.
	console.log(returnFullName(firstName, middleName, lastName));


	//You can also create a variable inside the function to contain the result and return that variable instead.
	function returnAddress(city, country){

		let fullAddress = city + ', ' + country;
		return fullAddress;

	};

	let myAddress = returnAddress("Pasig City", "Philippines");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		// console.log('Username: ' + username);
		// console.log('Level: ' + level);
		// console.log('Job: ' + job);

		return "Username: " + username + ' ' + "Level: " + level + ' ' + "Job: " + job
	};

	let user1 = printPlayerInfo("theTinker", 95, "Warrior");
	console.log(user1);

	

/*
	P E N D I N G

*/
	function promptName(name){
		prompt("name")
		return name
	};

	

	function promptNameAgain(promptName){
		promptName(name)
		console.log("hi" + name)
	};
 promptNameAgain(promptName);
